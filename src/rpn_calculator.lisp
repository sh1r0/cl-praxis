;; A RPN Calculator (Reverse Polar Notation)

(defun split (strng)
  (loop for i = 0 then (1+ j)
     as j = (position #\Space strng :start i)
     collect (subseq strng i j)
     while j))

(defun rpn-read ()
  (let ((strng (read-line)))
    (split strng)))

(defun rpn-process (str-lst)
  (let ((result 0)
        (val 0)
        (stack nil))
    (dolist (str str-lst)
      (setf val (read-from-string str))
      (if (numberp val)
          (progn (push result stack)
                 (setf result val))
          (setf result
                (funcall val (pop stack) result))))
    result))

(defun rpn-calc()
  (rpn-process (rpn-read)))
